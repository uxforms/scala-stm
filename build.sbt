
name := "scala-stm"

organization := "org.scala-stm"

version := "0.8-ba104ce"

scalaVersion := "2.11.6"

crossScalaVersions := Seq("2.10.5", "2.11.6")

libraryDependencies += ("org.scalatest" %% "scalatest" % "2.2.4" % "test")

libraryDependencies += ("junit" % "junit" % "4.12" % "test")

// skip exhaustive tests
testOptions += Tests.Argument("-l", "slow")

// test of TxnExecutor.transformDefault must be run by itself
parallelExecution in Test := false

enablePlugins(SbtOsgi)

osgiSettings

OsgiKeys.exportPackage := Seq("scala.concurrent.stm.*")

////////////////////
// publishing

pomExtra := {
  <url>http://nbronson.github.com/scala-stm/</url>
  <licenses>
    <license>
      <name>BSD</name>
      <url>https://github.com/nbronson/scala-stm/blob/master/LICENSE.txt</url>
      <distribution>repo</distribution>
    </license>
  </licenses>
  <scm>
    <connection>scm:git:git@github.com:nbronson/scala-stm.git</connection>
    <url>git@github.com:nbronson/scala-stm.git</url>
  </scm>
  <developers>
    <developer>
      <id>nbronson</id>
      <name>Nathan Bronson</name>
      <email>ngbronson@gmail.com</email>
    </developer>
  </developers>
}

publishMavenStyle := true



credentials += Credentials(Path.userHome / ".ivy2" / ".credentials")
